/*大屏*/
var geoCoordMap = {
/*
26.212063,107.488551
36.04701,103.924237
25.167859,104.950718
*/
    '贵阳': [106.633733, 26.646997],
    '安顺': [105.943193, 26.251512],
    '毕节': [105.292862, 27.281839],
    '遵义': [107.033435, 27.723366],
    '兴义': [104.950718, 25.167859],
    '都匀': [107.488551, 26.212063],
    '铜仁': [109.195515, 27.721307],
    '怀化': [110.005726, 27.564879],
    '长沙': [112.927822, 28.225277],
    '重庆': [106.565101, 29.569662],
    '深圳': [114.063576, 22.541287],
    '甘肃': [103.924237, 36.04701]
};

var BJData = [
    [{
        name: '贵阳'
    }, {
        name: '贵阳',
        value: 200
    }],
    [{
        name: '贵阳'
    }, {
        name: '安顺',
        value: 90
    }],
    [{
        name: '贵阳'
    }, {
        name: '毕节',
        value: 90
    }],
    [{
        name: '贵阳'
    }, {
        name: '遵义',
        value: 30
    }],
    [{
        name: '贵阳'
    }, {
        name: '都匀',
        value: 30
    }],
    [{
        name: '贵阳'
    }, {
        name: '兴义',
        value: 30
    }],
    [{
        name: '贵阳'
    }, {
        name: '铜仁',
        value: 100
    }],
    [{
        name: '贵阳'
    }, {
        name: '怀化',
        value: 40
    }],
    [{
        name: '贵阳'
    }, {
        name: '重庆',
        value: 40
    }],
    [{
        name: '贵阳'
    }, {
        name: '长沙',
        value: 50
    }],
    [{
        name: '贵阳'
    }, {
        name: '深圳',
        value: 30
    }],
    [{
        name: '贵阳'
    }, {
        name: '甘肃',
        value: 10
    }]
];

var planePath = 'path://M.6,1318.313v-89.254l-319.9-221.799l0.073-208.063c0.521-84.662-26.629-121.796-63.961-121.491c-37.332-0.305-64.482,36.829-63.961,121.491l0.073,208.063l-319.9,221.799v89.254l330.343-157.288l12.238,241.308l-134.449,92.931l0.531,42.034l175.125-42.917l175.125,42.917l0.531-42.034l-134.449-92.931l12.238-241.308L1705';

var convertData = function (data) {
    var res = [];
    for (var i = 0; i < data.length; i++) {
        var dataItem = data[i];
        var fromCoord = geoCoordMap[dataItem[0].name];
        var toCoord = geoCoordMap[dataItem[1].name];
        if (fromCoord && toCoord) {
            res.push([{
                coord: fromCoord
            }, {
                coord: toCoord
            }]);
        }
    }
    return res;
};

var color = ['#3ed4ff', '#ffa022', '#a6c84c'];
var series = [];
[
    ['贵阳', BJData]
].forEach(function (item, i) {
    series.push({
        name: item[0] + ' Top10',
        type: 'lines',
        zlevel: 1,
        effect: {
            show: true,
            period: 6,
            trailLength: 0.7,
            color: '#fff',
            symbolSize: 3
        },
        lineStyle: {
            normal: {
                color: color[i],
                width: 0,
                curveness: 0.2
            }
        },
        data: convertData(item[1])
    }, {
        name: item[0] + ' Top10',
        type: 'lines',
        zlevel: 2,
        effect: {
            show: true,
            period: 6,
            trailLength: 0,
            symbol: planePath,
            symbolSize: 15
        },
        lineStyle: {
            normal: {
                color: color[i],
                width: 1,
                opacity: 0.4,
                curveness: 0.2
            }
        },
        data: convertData(item[1])
    }, {
        name: item[0] + ' Top10',
        type: 'effectScatter',
        coordinateSystem: 'geo',
        zlevel: 2,
        rippleEffect: {
            brushType: 'stroke'
        },
        label: {
            normal: {
                show: true,
                position: 'right',
                formatter: '{b}'
            }
        },
        symbolSize: function (val) {
            return val[2] / 8;
        },
        itemStyle: {
            normal: {
                color: color[i]
            }
        },
        data: item[1].map(function (dataItem) {
            return {
                name: dataItem[1].name,
                value: geoCoordMap[dataItem[1].name].concat([dataItem[1].value])
            };
        })
    });
});

option = {
    backgroundColor: '#080a20',
    layoutCenter: ['30%', '-40%'],//位置
	layoutSize:'150%',//大小
    title: {
        left: 'left',
        textStyle: {
            color: '#fff'
        }
    },
    tooltip: {
        trigger: 'item'
    },
    legend: {
        orient: 'vertical',
        top: 'bottom',
        left: 'right',
        data: ['北京 Top10', '上海 Top10', '广州 Top10'],
        textStyle: {
            color: '#fff'
        },
        selectedMode: 'single'
    },
    geo: {
        map: 'china',
        zoom: 3.2,
        label: {
            emphasis: {
                show: false
            }
        },
        roam: true,
        itemStyle: {
            normal: {
                areaColor: '#142957',
                borderColor: '#0692a4'
            },
            emphasis: {
                areaColor: '#0b1c2d'
            }
        }
    },
    series: series
};
var myecharts = echarts.init($('.map .geo')[0])
myecharts.setOption(option)