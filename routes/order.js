var express = require('express');
var router = express.Router();
const {connection} = require('../utils/MySql')

/* GET home page. */
router.get('/apartment_good', function(req, res, next) {
    var sql = 'SELECT ognization,good_name,sum(count)\n' +
        'from good\n' +
        'GROUP BY ognization,good_name\n' +
        'ORDER BY ognization,sum(count)\n' +
        'DESC'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});
router.get('/', function(req, res, next) {
    var sql = 'SELECT count(*),sum(count),sum(total_price)\n' +
        'FROM good'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});
router.get('/apartment', function(req, res, next) {
    var sql = 'SELECT ognization,sum(count),sum(total_price)\n' +
        'from good\n' +
        'GROUP BY ognization\n' +
        'ORDER BY sum(total_price)\n' +
        'DESC'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});
router.get('/guest', function(req, res, next) {
    var sql = 'SELECT guest,sum(count),sum(total_price) \n' +
        'from good\n' +
        'GROUP BY guest\n' +
        'ORDER BY sum(total_price)\n' +
        'DESC\n' +
        'LIMIT 50'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});
router.get('/channel', function(req, res, next) {
    var sql = 'SELECT type,count(store_name) as store,\n' +
        '(count(store_name)/(SELECT count(store_name) from channel))*100 as account\n' +
        'FROM channel\n' +
        'GROUP BY type\n' +
        'ORDER BY count(store_name)\n' +
        'DESC'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});
router.get('/milk10', function(req, res, next) {
    var sql = 'SELECT good_name,sum(count)\n' +
        'from good\n' +
        'GROUP BY good_name\n' +
        'ORDER BY sum(count)\n' +
        'DESC\n' +
        'LIMIT 10'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});

module.exports = router;
