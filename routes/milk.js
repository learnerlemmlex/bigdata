var express = require('express');
var router = express.Router();
const {connection} = require('../utils/MySql')
// " + req.query['name'] + "
/* GET home page. */
router.get('/guiyang', function(req, res, next) {
    var sql = 'SELECT ognization,good_name,sum(count)\n' +
        'from good\n' +
        'WHERE ognization LIKE \'贵阳%\'\n' +
        'GROUP BY ognization,good_name\n' +
        'ORDER BY ognization,sum(count)\n' +
        'DESC\n' +
        'LIMIT 6'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});
router.get('/nanfang', function(req, res, next) {
    var sql = 'SELECT ognization,good_name,sum(count)\n' +
        'from good\n' +
        'WHERE ognization LIKE \'%南方%\'\n' +
        'GROUP BY ognization,good_name\n' +
        'ORDER BY ognization,sum(count)\n' +
        'DESC\n' +
        'LIMIT 6'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});router.get('/zunyi', function(req, res, next) {
    var sql = 'SELECT ognization,good_name,sum(count)\n' +
        'from good\n' +
        'WHERE ognization LIKE \'遵义%\'\n' +
        'GROUP BY ognization,good_name\n' +
        'ORDER BY ognization,sum(count)\n' +
        'DESC\n' +
        'LIMIT 6'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});router.get('/kaili', function(req, res, next) {
    var sql = 'SELECT ognization,good_name,sum(count)\n' +
        'from good\n' +
        'WHERE ognization LIKE \'凯里%\'\n' +
        'GROUP BY ognization,good_name\n' +
        'ORDER BY ognization,sum(count)\n' +
        'DESC\n' +
        'LIMIT 6'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});router.get('/duyun', function(req, res, next) {
    var sql = 'SELECT ognization,good_name,sum(count)\n' +
        'from good\n' +
        'WHERE ognization LIKE \'都匀%\'\n' +
        'GROUP BY ognization,good_name\n' +
        'ORDER BY ognization,sum(count)\n' +
        'DESC\n' +
        'LIMIT 6'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});router.get('/anshun', function(req, res, next) {
    var sql = 'SELECT ognization,good_name,sum(count)\n' +
        'from good\n' +
        'WHERE ognization LIKE \'安顺%\'\n' +
        'GROUP BY ognization,good_name\n' +
        'ORDER BY ognization,sum(count)\n' +
        'DESC\n' +
        'LIMIT 6'
    connection.query(sql,function(err,result){
        if(err){
            console.log('select error.',err.message)
            return;
        }
        var dataString =JSON.stringify(result)
        var dataJson = JSON.parse(dataString)
        res.send(dataJson)
    })
});



module.exports = router;
